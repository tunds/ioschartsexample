//
//  ViewController.swift
//  playingWiosCharts
//
//  Created by Tunde Adegoroye on 07/01/2017.
//  Copyright © 2017 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController {

    @IBOutlet weak var chartGradientContainer: UIView!
    @IBOutlet weak var chartContainer: LineChartView!
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
    let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 3.0]

    // TODO: Allow scrolling and panning
    // TODO: Spacing around chart
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Set up gradient
        let layer = CAGradientLayer()
        
        let start = UIColor(red:1.00, green:0.90, blue:0.57, alpha:1.00).cgColor as CGColor
        let end = UIColor(red:0.99, green:0.55, blue:0.32, alpha:1.00).cgColor as CGColor
        
 
        layer.frame = chartGradientContainer.bounds
        
        layer.colors = [start, end]
        
        chartGradientContainer.layer.insertSublayer(layer, at: 0)
        
        setChart(dataPoints: months, values: unitsSold)

        
        
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        // Set up x axis
        let xaxis = XAxis()
        let chartFormatter = ChartFormatter(myArr: months)
        
        // Array for entries
        var dataEntries: [ChartDataEntry] = []

        // Add new data entries
        for i in 0..<dataPoints.count {
        
            let dataEntry = ChartDataEntry(x: Double(i) , y: values[i])
            
            dataEntries.append(dataEntry)
            chartFormatter.stringForValue(Double(i), axis: xaxis)
        }
        
        xaxis.valueFormatter = chartFormatter
        chartContainer.xAxis.valueFormatter = xaxis.valueFormatter
        
        // Configure the dataset
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: "Units Sold")
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        
        // Hide gridlines
        chartContainer.drawGridBackgroundEnabled = false
        chartContainer.xAxis.drawGridLinesEnabled = false
        chartContainer.rightAxis.drawGridLinesEnabled = false
        chartContainer.leftAxis.drawGridLinesEnabled = false
        
        // Hide Axis lines
        chartContainer.leftAxis.drawAxisLineEnabled = false
        chartContainer.xAxis.drawAxisLineEnabled = false
        
        // Hide legend & info
        chartContainer.legend.enabled = false
        chartContainer.chartDescription?.enabled = false
        
        // Hide right axis and move label to the bottom
        chartContainer.xAxis.labelPosition = .bottom
        chartContainer.rightAxis.enabled = false
        chartContainer.leftAxis.enabled = false
        
        // Hide horizontal lines
        lineChartDataSet.drawHorizontalHighlightIndicatorEnabled = false
        lineChartDataSet.drawVerticalHighlightIndicatorEnabled = false
        
        // Remove duplication
        chartContainer.xAxis.granularityEnabled = true
        chartContainer.xAxis.granularity = 1.0
 
        
        // General styles
        chartContainer.backgroundColor = UIColor(red:1, green:1, blue:1, alpha:0)
        chartContainer.isOpaque = false
        lineChartDataSet.mode = .cubicBezier
        lineChartDataSet.circleRadius = 6
        lineChartDataSet.circleHoleRadius = 0
        lineChartDataSet.lineWidth = 3.5
        lineChartDataSet.setCircleColor(.white)
        lineChartDataSet.setColor(.white)
        
 
        // Interaction config
        chartContainer.setScaleEnabled(false)
        chartContainer.doubleTapToZoomEnabled = false
        chartContainer.pinchZoomEnabled = false
        
        chartContainer.zoom(scaleX: 1.05, scaleY: 1.05, x: 0, y: CGFloat(unitsSold[0]))
        
        // Animate
        chartContainer.animate(yAxisDuration: 1.5, easingOption: .easeInOutCubic)
        
        chartContainer.data = lineChartData
    

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

@objc(ChartFormatter)
class ChartFormatter: NSObject, IAxisValueFormatter {

    private var myArr: [String]!
    
    init(myArr: [String]) {
        
        
        self.myArr = myArr
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        let val = Int(value)
        
        if val >= 0 && val <= myArr.count {
            
            return myArr[Int(val)]
            
        }
        
        return ""
        
    }
}
